$(document).ready(function(){
    $("#submitIt").click(function(){  
        var inside = ($("#name").val());
        var mayPass = true;

    if(inside == ""){
            alert("Name must be filled out");
            mayPass = false;
    }


        inside = ($("#sname").val());

    if(inside == ""){
        alert("Second name must be filled out");
        mayPass = false;
    }

    inside = ($("#email").val());

    if(inside == ""){
        alert("E-mail adress must be filled out");
        mayPass = false;
    }
    else{
        if (inside.indexOf('@') == -1) {
            alert("Please insert a valid e-mail adress.");
            mayPass = false;
         }
    }

    inside = ($("#pass").val());

    if(inside == ""){
        alert("Password  must be filled out");
        mayPass = false;
    }
    else{
        
        if(!hasLowerCase(inside) || !hasUpperCase(inside) || !hasNumber(inside) || (inside.length < 7))  
        {
            alert("Your password must be above 8 characters. It must contain a mixture of upper and lower case letters, and at least one number.");
            mayPass = false;
        }
    }

        if(mayPass)
        window.location.href = "Home.html";
    });    




    function hasLowerCase(str) {
        return (/[a-z]/.test(str));
    }
    function hasUpperCase(str) {
        return (/[A-Z]/.test(str));
    }
    function hasNumber(str) {
        return (/[0-9]/.test(str));
    }
});